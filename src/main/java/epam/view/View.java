package epam.view;

import epam.controller.TaskRunner;

public class View {
    public static void main(String[] args) {
        TaskRunner taskRunner = new TaskRunner();
        taskRunner.run();
    }
}
