package epam.config;

import epam.model.*;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("bean.properties")
@Import(Config2.class)
public class Config1 {

    @Bean
    @DependsOn({"beanB","beanC"})
    public BeanA getBeanBC(BeanB getBeanB,BeanC getBeanC){
        return new BeanA(getBeanB,getBeanC);
    }
    @Bean
    public BeanA getBeanBD(BeanB getBeanB,BeanD getBeanD){
        return new BeanA(getBeanB,getBeanD);
    }
    @Bean
    public BeanA getBeanCD(BeanC getBeanC,BeanD getBeanD){
        return new BeanA(getBeanC,getBeanD);
    }
    @Bean(name = "beanB",initMethod = "init", destroyMethod = "destroy")
    public BeanB getBeanB(){
        return new BeanB();
    }

    @Bean(name = "beanC",initMethod = "init", destroyMethod = "destroy")
    public BeanC getBeanC(){
        return new BeanC();
    }
    @Bean(name = "beanD",initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD(){
        return new BeanD();
    }

    @Bean
    public BeanE getBeanE(){
        return new BeanE();
    }
    @Bean
    public BeanE getBeanE1(BeanA getBeanBC){
        return new BeanE(getBeanBC);
    }
    @Bean
    public BeanE getBeanE2(BeanD getBeanD){
        return new BeanE(getBeanD);
    }
    @Bean
    @Lazy
    public BeanF getBeanF(){
        return new BeanF();
    }
    @Bean
    public static BeanFactoryPostProcessor beanFactoryPostProcessor() {
        return new MyBeanPostFactoryProccesor();
    }

    @Bean
    public BeanPostProcessor beanPostProcessor() {
        return new MyBeanPostProcessor();
    }






}
