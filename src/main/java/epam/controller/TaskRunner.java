package epam.controller;

import epam.config.Config1;
import epam.model.BeanA;
import epam.model.BeanF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TaskRunner implements Runner {
    private static final Logger logger = LogManager.getLogger();

    public void run() {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(Config1.class);
        BeanA beanBC = context.getBean("getBeanBC", BeanA.class);
        BeanA beanDC = context.getBean("getBeanBD", BeanA.class);
        BeanA beanCD = context.getBean("getBeanCD", BeanA.class);
        BeanF beanF = context.getBean(BeanF.class);
        logger.info(beanBC.getName() + " " + beanBC.getValue());
        logger.info(beanDC.getName() + "  " + beanDC.getValue());
        logger.info(beanCD.getName() + " " + beanCD.getValue());
        context.close();
    }
}
