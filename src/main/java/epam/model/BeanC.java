package epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

public class BeanC implements BeanValidator {
    private static final Logger logger = LogManager.getLogger();
    @NonNull
    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private int value;

    @Override
    public void validate() {
        if (getValue() >= 0) {
            logger.info("Validate successful : C");
        } else {
            logger.error("Validate error");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void destroy() {
        logger.info("Destroy C");
    }

    public void init() {
        logger.info("init C");
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
