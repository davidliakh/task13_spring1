package epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;

public class BeanE implements BeanValidator {
    private static final Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    @Override
    public void validate() {
        if (getValue() >= 0) {
            logger.info("Validate successful : E");
        } else {
            logger.error("Validate error");
        }
    }
    @PostConstruct
    private void postConstruct(){
    logger.info("PostConstruct: E");
    }
    public BeanE(BeanA beanBC) {
        this.name = beanBC.getName();
        this.value = beanBC.getValue();
    }
    public BeanE(BeanD beanD) {
        this.name = beanD.getName();
        this.value = beanD.getValue();
    }

    public BeanE() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
