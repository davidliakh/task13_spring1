package epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;

public class BeanB implements BeanValidator {
    private static final Logger logger = LogManager.getLogger();
    @NonNull
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;

    @Override
    public void validate() {
        if (getValue() >= 0) {
            logger.info("Validate successful : B");
        } else {
            logger.error("Validate error");
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void destroy() {
        logger.info("Destroy B");
    }

    public void init() {
        logger.info("init B");
    }
    public void myCustomInitMethod() {
        logger.info("Text From changed init method" + getClass().getSimpleName());
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
