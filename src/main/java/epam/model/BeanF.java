package epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanF implements BeanValidator {
    private static final Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    @Override
    public void validate() {
        if (getValue() >= 0) {
            logger.info("Validate successful : F");
        } else {
            logger.error("Validate error");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
