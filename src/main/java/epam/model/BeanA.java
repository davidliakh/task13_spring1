package epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PreDestroy;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private static final Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    @Override
    public void validate() {
        if (getValue() >= 0) {
            logger.info("Validate successful : A");
        } else {
            logger.error("Validate error");
        }
    }

    public void afterPropertiesSet() {
        logger.info("afterPropertiesSet method from interface ...");
    }

    public void destroy() {
        logger.info("destroy from interface DisposableBean");
    }

    public BeanA(BeanB beanB, BeanC beanC) {
        this.name = beanB.getName();
        this.value = beanC.getValue();
    }

    public BeanA(BeanB beanB, BeanD beanD) {
        this.name = beanB.getName();
        this.value = beanD.getValue();
    }

    public BeanA(BeanC beanC, BeanD beanD) {
        this.name = beanC.getName();
        this.value = beanD.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @PreDestroy
    public void preDestroy() {
        logger.info("Pre Destroy : E");
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
